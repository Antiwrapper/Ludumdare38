extends Node

func _restart():
	get_tree().get_root().get_child(get_tree().get_root().get_child_count() - 1).queue_free();
	var s = preload("res://Scenes/Root.tscn").instance();
	get_tree().get_root().add_child(s);
	get_tree().set_current_scene(s);