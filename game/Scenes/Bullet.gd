extends KinematicBody2D

const MAX_SPEED = 400;
var Dir = Vector2();
var Life = 10;

func _ready():
	set_fixed_process(true);

func _fixed_process(delta):
	Life -= delta;
	if (Life < 0):
		queue_free();
	move(delta * Dir * MAX_SPEED);
	if (is_colliding()):
		if (get_collider().get_name().begins_with("Enemy") || get_collider().get_name().begins_with("@Enemy")):
			get_collider().queue_free();
		queue_free();