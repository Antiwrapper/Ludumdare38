extends StaticBody2D

const WALLS = 100;

func _ready():
	for i in range(0, WALLS):
		var s = preload("res://Scenes/Wall.tscn").instance();
		add_child(s);
	set_process(true);

func _process(delta):
	if (Input.is_action_pressed("Restart")):
		if (get_parent().has_node("Player")):
			if (get_parent().get_node("Player").Score < 2):
				return;
		get_node("/root/Global")._restart();