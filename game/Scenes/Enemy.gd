extends KinematicBody2D

var Speed = 200;

func _ready():
	Speed = rand_range(200, 300);
	set_fixed_process(true);

func _fixed_process(delta):
	if (get_parent().has_node("Player") == false):
		return;
	var dir = get_node("../Player").get_pos() - get_pos();
	dir = dir.normalized();
	var motion = move(Speed * dir * delta);
	if (is_colliding()):
		var node = get_collider();
		if (node.get_name().begins_with("Player")):
			get_node("../Camera/Text").set_text("Final score: " + var2str(stepify(node.Score, 0.1)) + " seconds\n Press R to restart");
			node.Dead = true;
			node.queue_free();
		var n = get_collision_normal();
		motion = n.slide(motion);
		move(motion);

