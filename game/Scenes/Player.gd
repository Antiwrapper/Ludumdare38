extends KinematicBody2D

const MAX_SPEED = 200;
const VERTICAL = 300;
const GRAVITY = 500;
const SINK_SPEED = 200;
const ENEMY_SPAWN = 4;
var Dir_Gravity = Vector2();
var Jump = false;
var Gravity = 0;
var Pressed = false;
var Score = 0;
var Dead = false;
var EnemyCooldown = 2;

func _ready():
	set_process(true);
	set_fixed_process(true);

func _process(delta):
	if (Input.is_action_pressed("Shoot")):
		if (Pressed == false):
			_shoot();
			Pressed = true;
	else:
		if (Pressed == true):
			Pressed = false;

func _fixed_process(delta):
	Score += delta;
	Dir_Gravity = (get_parent().get_node("Planet").get_pos() - get_pos()).normalized();
	set_rot(Dir_Gravity.angle());
	Gravity += GRAVITY * delta;
	EnemyCooldown -= delta;
	if (EnemyCooldown <= 0):
		EnemyCooldown = ENEMY_SPAWN - (Score / 15);
		if (EnemyCooldown < 0.3):
			EnemyCooldown = 0.3;
		var e = preload("res://Scenes/Enemy.tscn").instance();
		var t = -Dir_Gravity * rand_range(800, 1000) + get_node("../Planet").get_pos() + Vector2(rand_range(-100, 100), rand_range(-100, 100));
		e.set_pos(t);
		get_parent().add_child(e);
	
	var angle = Dir_Gravity.angle();
	var dir = Vector2();
	
	if (Input.is_action_pressed("left")):
		dir.x -= 1;
	if (Input.is_action_pressed("right")):
		dir.x += 1;
	if (Input.is_action_pressed("jump") && Jump == true):
		Gravity = -VERTICAL;
		Jump = false;
	
	dir = dir.normalized();
	dir = dir.rotated(angle);
	
	var motion = (dir * MAX_SPEED + (Gravity * Dir_Gravity)) * delta;
	motion = move(motion);
	
	var i = 0;
	while (is_colliding() && i < 2):
		var node = get_collider();
		if (node.has_node("Shape")):
			var shape = node.get_node("Shape");
			shape.set_pos(shape.get_pos() + Vector2(0, SINK_SPEED * delta));
			node.get_node("Sprite").set_pos(shape.get_pos());
		elif (node.has_node("Circle")):
			get_node("../Camera/Text").set_text("Final score: " + var2str(stepify(Score, 0.1)) + " seconds\n Press R to restart");
			Dead = true;
			queue_free();
		elif (node.get_name().begins_with("Enemy") || node.get_name().begins_with("@Enemy")):
			get_node("../Camera/Text").set_text("Final score: " + var2str(stepify(Score, 0.1)) + " seconds\n Press R to restart");
			Dead = true;
			queue_free();
		var n = get_collision_normal();
		if ((n + Dir_Gravity).length() < 0.1):
			Gravity = 0;
			Jump = true;
		
		motion = n.slide(motion);
		motion = move(motion);
		i += 1;
	
	if (Dead == false):
		get_node("../Camera").set_pos(get_pos());
		get_node("../Camera").set_rot(get_rot());
		get_node("../Camera/Text").set_text("Time: " + var2str(stepify(Score, 0.1)));

func _shoot():
	var b = preload("res://Scenes/Bullet.tscn").instance();
	b.set_pos(get_pos());
	b.Dir = (get_global_mouse_pos() - get_pos()).normalized();
	get_parent().add_child(b);