extends StaticBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	set_rot(rand_range(0.0, 2.0 * PI));
	get_node("Sprite").set_pos(Vector2(0, rand_range(-100, -200)));
	get_node("Shape").set_pos(get_node("Sprite").get_pos());